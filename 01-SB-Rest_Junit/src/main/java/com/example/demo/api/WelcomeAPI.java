package com.example.demo.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.User;
import com.example.demo.service.WelcomeService;

@RestController
@RequestMapping(value="/api")
public class WelcomeAPI 
{
	@Autowired
	WelcomeService service;
	
	@GetMapping(value="/welcome")
	public ResponseEntity<?> getWelcome()
	{
		String msg=service.getWelcomeMsg();
		ResponseEntity<String> res=new ResponseEntity<>(msg,HttpStatus.OK);
		return res;
		
  }
	
	@PostMapping(value="/user/add")
	public ResponseEntity<?> addUser(@RequestBody User user)
	{
		boolean b=service.addUser(user);
		if(b)
		{
			return new ResponseEntity<String>("user added",HttpStatus.CREATED);
		}
		else {
			return new ResponseEntity<String>("user not added",HttpStatus.BAD_REQUEST);
		}
		
	}
	@PutMapping(value="/update")
	public ResponseEntity<?> updateUser(@RequestBody User user)
	{
		boolean b=service.updateUser(user);
		if(b==true)
		{
			return new ResponseEntity<String>("updated",HttpStatus.ACCEPTED);
		}
		else
		{
			return new ResponseEntity<String>("not added",HttpStatus.BAD_REQUEST);
		}
		
	}

}
