package com.demo.service.impl;

import com.demo.dao.UserDAO;
import com.demo.service.UserServiec;

public class UserServiceImpl implements UserServiec 
{
	UserDAO dao;
	public UserServiceImpl(UserDAO dao)
	{
		this.dao=dao;
	}
	
	

	@Override
	public String findNameById(Integer id) 
	{
		String n=dao.fetchNameById(id);
	
		return n;
	}

	@Override
	public String findCityById(Integer id) {
		String c=dao.fetchCityById(id);
		return c;
	}
	@Override
	public String addReview(Integer id, String review) {
		dao.writeReview(id, review);
		return "success";
	}

}
