package com.demo.service;

public interface UserServiec 
{
	String findNameById(Integer id);
	String findCityById(Integer id);
	String addReview(Integer id,String review);

}
