package com.demo.test;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class IsPalindromeTest 
{
	@BeforeAll
	public static void beforeAllMethod()
	{
		System.out.println("before all the method it will ");
	}
	@AfterAll
	public static void afterAllMethod()
	{
		System.out.println("after all method is called after all the test method only once");
	}
	@AfterEach
	public void tearDown()
	{
		System.out.println("teardown()  called");
	}
	@BeforeEach
	public void setUp()
	{
		System.out.println("setup() called");
	}
	@ParameterizedTest
	@ValueSource(strings = {"madam","antra","radar","racecar"})
	public void isPalindromeCheck(String s)
	{
				assertTrue(new IsPalidrome().IsPalidrome(s));
	}

}
